const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require('fs');
const fetch = require('node-fetch');
const cheerio = require('cheerio');


app.use(bodyParser.json());


let dealersJson = {
  dealer: "",
  address: "",
  name: "",
  number: ""
}

let dealersArray = []

fetch('http://piranigroup.com.pk/wp-content/themes/sp-wordpress/DisplayDealer.php?city=Karachi')
  .then(res => res.text())
  .then((body) => {
    const $ = cheerio.load(body)
    for (let i = 0; i < ($("tbody").find("tr").length); i++) {
      let content = ($('tr').slice(i, i + 1).find('p').html().replace("<br>", ",").split(","))
      let dealer = content.shift(); //remove first element and return
      let contact = content.pop().trim(); //remove last element and return
      contact = contact.trim().split(" ");
      let number = "+" + contact.pop().trim(); //remove last element and return
      let name = contact.join(" ").trim();
      let address = content.join().trim()


      // console.log(`Comapny:${dealer} Number:${number} Address:${address} Name:${name}`)
      dealersJson.dealer = dealer;
      dealersJson.number = number;
      dealersJson.address = address;
      dealersJson.name = name;
      dealersArray.push(dealersJson)
    }
  });

const allDealers = dealersArray;
const filename = "Karachi.json";
fs.writeFileSync(filename, allDealers);
const port = process.env.PORT || 8000;

app.listen(port, () => console.log(`Listening on port ${port}`));